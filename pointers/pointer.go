package pointers

var a int = 1
var p *int = &a
// var p = &a
// *p == a
// *p != a
//  p == &a
//  p != a

// p: new(int)
// *p = 10

// var a = 1
// var p = &a
// var pp = &p

//  *pp =  p = &a
// **pp = *p = a
