package numbers

import (
	"errors"
)

func Operations(x, y int) (xx ,yy int, xy float64, err error) {
	if y == 0 {
		err = errors.New("cannot be 0")
		return 0, 0, 0, err
	}
	xx = x + y
	yy = x * y
	xy = float64(x) / float64(y)
	return xx, yy, xy, nil
}