package slices

var (
	S = []int{1,2,3}
	a = [6]int{10, 20, 30, 40, 50, 60}
	s = a[1:4]
	K = make([]int, 5, 10)

	src = []string{"Sublime", "VSCode", "IntelliJ", "Eclipse"}
	dest = make([]string, 2)

	numElementsCopied = copy(dest, src)


	// slice1 := []string{"C", "C++", "Java"}
	// slice2 := append(slice1, "Python", "Ruby", "Go")


)
	// T := []int{1,2,3,4,5,6}