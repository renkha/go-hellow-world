package main

import (
	"fmt"
	// "errors"
	"go-hellow-world/arrays"
	"go-hellow-world/looping"
	"go-hellow-world/numbers"
	"go-hellow-world/slices"
	"go-hellow-world/strings/profile"
	"go-hellow-world/strings/saysomethings"
)

func main() {

	fmt.Println(saysomethings.Hello)
	fmt.Println(saysomethings.MorningText)
	fmt.Println(profile.Title)
	fmt.Printf("My name : %s %s, Age: %d\n", profile.Name, profile.Lastname, profile.Age)
	fmt.Printf("Address : %T, Phone: %T\n", profile.Address, profile.Phone)
	fmt.Println("Email :", profile.Email)

	fmt.Println("Try if else")
	fmt.Println(looping.TryIfWithEvenOrOddNumber(9))

	fmt.Println("Try switch")
	fmt.Println(looping.TrySwitchWithWhatADay(4))

	fmt.Println("Try for")
	fmt.Printf("First positive number divisible by both 3 and 5 is %d\n\n", looping.TryForWithDiveded(15))

	fmt.Println("Try function")
	a := 1
	b := 2
	c := numbers.IsPrime(19)
	x, y, xy, _ := numbers.Operations(a, b)
	fmt.Println(c, x, y, xy)

	fmt.Println("Try Single Dimension Array")
	arrays.ArraySD_a[2] = "c"
	fmt.Println(arrays.ArraySD_a)
	for i := 0; i < len(arrays.ArraySD_a); i++ {
		fmt.Println(arrays.ArraySD_a[i])
	}

	fmt.Println(arrays.ArraySD_b)
	sum := int(0)
	for i := 0; i < len(arrays.ArraySD_b); i++ {
		sum = sum + arrays.ArraySD_b[i]
	}
	fmt.Println("Jumlah array value from ArraySD_b:", sum)

	fmt.Println(arrays.ArraySD_c)
	for _, value := range arrays.ArraySD_c {
		fmt.Println(value)
	}
	// var sum_index int
	// for index, _ := range arrays.ArraySD_c {
	// 	sum_index = sum_index + index
	// }
	// fmt.Printf("Jumlah array index from ArraySD_c: %d\n\n", sum_index)

	fmt.Println("Try Multi Dimension Array")
	fmt.Println(arrays.ArrayMD_a)
	for _, value := range arrays.ArrayMD_a {
		fmt.Println(value)
	}
	for i := 0; i < len(arrays.ArrayMD_a); i++ {
		for _, value := range arrays.ArrayMD_a[i] {
			fmt.Println(value)
		}
	}

	fmt.Println("Try Slice Array")

	fmt.Println(slices.S)
	// fmt.Println(T)
}
