package methods

type Point struct{
	X float64
}
// type Point string

func (p Point) Bigger(y float64) bool {
	return p.X < y
}
var p = Point{3}
// p := Point{3}
// p.Bigger(1) // false

func Bigger(p Point, y float64) bool {
	return p.X < y
}
// Bigger(p, 1)
	
func (p *Point) Add(y float64) {
	p.X = p.X + y
}
// p.Add(4) // 7

func Add(p *Point, y float64) {
	p.X = p.X + y
}
// Add(&p, 4) // 7