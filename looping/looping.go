package looping

import (
	// "fmt"
	// "errors"
)

func TryIfWithEvenOrOddNumber(n int) string {
	if n%2 == 0 {
		return "Even number\n"
	} else {
		return "Odd number\n" 
	}
}

func TrySwitchWithWhatADay(d int) string {
	switch d {
		case 1, 2, 3, 4, 5: 
			return "Weekday\n"
		case 6, 7: 
			return "Weekend\n"
		default: return "...\n"
	}
}

	// for i := 0; i <= 10; i+=2 {
	// 	fmt.Println(i)
	// }

	// for d <= 50 {
	// 	fmt.Println(d)
	// 	d*=2
	// }

func TryForWithDiveded(n int) int {
	for num := 1; num <= n; num++ {
		if num%3 == 0 && num%5 == 0 {
			return num
			break
		}
	}
	return 0
}

	// for num := 1; num <= 10; num++ {
	// 	if num%2 == 0 {
	// 		continue;
	// 	}
	// 	fmt.Printf("%d \n", num)
	// }