package structs

type S struct{
	Id int // Id, Age int
	Age int
	Name string
}

var P = S{
	Id: 1,
	Age: 111,
	Name: "Asd",
}

// var P = S{Name: "Asd"}// all uninitialized = zero value
// var P = S{}// all uninitialized = zero value


// P := S{
// 	Id: 1,
// 	Age: 111,
// 	Name: "Asd",
// }

// fmt.Println(S.Name) // Asd

// ps = &P
// fmt.Println(ps) // &{1, 111, "Asd"}
// (*ps).Name == ps.Name == "Asd"

// sn := new(S)
// sn.Name = "Zxc"
// fmt.Println(sn) // &{0, 0, Zxc}
// fmt.Println(sn.Name) // Zxc

