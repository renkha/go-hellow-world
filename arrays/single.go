package arrays

import (
	// "fmt"
)

var (
	ArraySD_a = [3]string{"a","b"}
	ArraySD_b = [...]int{3, 5, 7, 9, 11, 13, 17}
	ArraySD_c = [7]string{"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"}
)
// ArraySD_b := [...]int{3, 5, 7, 9, 11, 13, 17}
