package interfaces

type Operation interface {
	Plus() float64
	Minus() float64
	Multiple() float64
	Divided() float64
}

type Number struct {
	X, Y float64
}
func (n Number) Plus() float64 {
	return n.X + n.Y
}
func (n Number) Minus() float64 {
	return n.X - n.Y
}
func (n Number) Multiple() float64 {
	return n.X * n.Y
}
func (n Number) Divided() float64 {
	return n.X / n.Y
}

var n Operation = Number{1.0, 2.0} 